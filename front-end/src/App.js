import {
  Alert,
  Button,
  Container,
  Divider,
  ListItem,
  Paper,
  Stack,
} from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import AlertComponent from "./components/AlertComponent";
import Wall from "./components/Wall";

function App() {
  const [walls, setWalls] = useState({
    wall1: { height: 0, window: 0, width: 0, door: 0 },
    wall2: { height: 0, window: 0, width: 0, door: 0 },
    wall3: { height: 0, window: 0, width: 0, door: 0 },
    wall4: { height: 0, window: 0, width: 0, door: 0 },
  });

  const [errors, setErrors] = useState({
    wall1: { wallsize: "", doorWindow: "", validHeighDoor: "" },
    wall2: { wallsize: "", doorWindow: "", validHeighDoor: "" },
    wall3: { wallsize: "", doorWindow: "", validHeighDoor: "" },
    wall4: { wallsize: "", doorWindow: "", validHeighDoor: "" },
  });

  const [roomTotal, setRoomTotal] = useState({
    squareMeter: 0,
    window: 0,
    door: 0,
  });

  const [showAlert, setShowAlert] = useState(false);
  const [errorsToPrint, setErrorsToPrint] = useState("");
  const [cans, setCans] = useState([]);
  const keys = Object.keys(walls);
  const errorsKeys = Object.keys(errors.wall1);

  const paperStyleComponent = {
    textAlign: "center",
    height: { xs: "50px", lg: "100px" },
    width: { lg: "200px" },
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  };

  //  função que verifica se a parede tem menos de 1m² e nais de 15m²
  function squareMeterValidation() {
    for (const key of keys) {
      let squareMeters = walls[key].height * walls[key].width; //tamanho parede em m²

      if (!(squareMeters > 1 && squareMeters <= 15)) {
        setErrors((prevState) => {
          const aux = { ...prevState };
          aux[key].wallsize = `A área da parede ${key.slice(
            key.length - 1
          )} deve ser maior que 1m² e menor que 15m². O valor atual é ${squareMeters}m²`;

          return aux;
        });
      } else {
        setErrors((prevState) => {
          const aux = { ...prevState };
          aux[key].wallsize = "";

          return aux;
        });
      }
    }
  }
  // função que verifica se a area total das janelas/portas excede 50% da area da parede
  function doorWindowAreaValidation() {
    let totalDoorArea = 0;
    let totalWindowArea = 0;
    let total = 0;
    for (const key of keys) {
      const doorArea = 0.8 * 1.9 * walls[key].door; //tamanho da porta em m²
      const windowArea = 1.2 * 2 * walls[key].window; //taamanho da janela em m²
      const wallArea = walls[key].height * walls[key].width;

      if (doorArea + windowArea >= wallArea / 2) {
        setErrors((prevState) => {
          const aux = { ...prevState };
          aux[key].doorWindow = `Verifique a parede ${key.slice(
            key.length - 1
          )}. Janelas e portas devem ocupar 50% ou menos da área de parede.  Área de parede: ${wallArea}m² Portas/Janelas: ${
            doorArea + windowArea
          }m²`;

          return aux;
        });
      } else {
        totalDoorArea += doorArea;
        totalWindowArea += windowArea;
        total += wallArea;
        setErrors((prevState) => {
          const aux = { ...prevState };
          aux[key].doorWindow = "";

          return aux;
        });
      }
    }
    setRoomTotal({
      ...roomTotal,
      window: totalWindowArea,
      door: totalDoorArea,
      squareMeter: total,
    });
  }
  // função que verifica se a parede tem altura mínima de 2,20m(1.90m da porta + 0.30m de altura mínima) para ter uma porta
  function doorHeighValidation() {
    for (const key of keys) {
      if (walls[key].door >= 1 && walls[key].height < 2.2) {
        setErrors((prevState) => {
          const aux = { ...prevState };
          aux[key].validHeighDoor = `A parede ${key.slice(
            key.length - 1
          )} possui porta(s) e sua altura mínima deve ser 2.20m`;

          return aux;
        });
      } else {
        setErrors((prevState) => {
          const aux = { ...prevState };
          aux[key].validHeighDoor = "";

          return aux;
        });
      }
    }
  }

  // função responsável pelo calculo das latas de tinta necessárias para pintar a parede
  function paintQtt() {
    //calculo para quantidade de tinta em litros, total m² - janelas+portas. O valor é divido por 5 pois 1l tinta pinta 5 m²
    let paintLiters =
      (roomTotal.squareMeter - (roomTotal.door + roomTotal.window)) / 5;

    const cansSize = [18, 3.6, 2.5, 0.5];

    // logica para calcular quantiade de latas, o retorno do map é um array de duas posições onde o primeiro
    // elemento é o tamanho da lata e o segundo a quantidade de latas
    const cansQtt = cansSize
      .map((value) => {
        if (paintLiters / value > 1) {
          let cans = Math.floor(paintLiters / value);
          paintLiters = paintLiters - cans * value;

          return [value, cans];
        }
      })
      .filter((element) => element != undefined);
    //montagem do componente <paper> </paper> que irá dentro do stack -> linha 216
    const cansString = cansQtt.map((element, index) => {
      return (
        <Paper
          sx={paperStyleComponent}
          elevation={5}
        >{`Você irá precisar de ${element[1]} Lata(s) de ${element[0]} litros`}</Paper>
      );
    });

    return cansString;
  }

  // função auxiliar para pegar todos os textox de erros do objeto errors
  function getErrorsText() {
    const arr = [];
    for (const key of keys) {
      for (const errorKey of errorsKeys) {
        if (errors[key][errorKey]) {
          arr.push(errors[key][errorKey]);
        }
      }
    }

    return arr;
  }

  // função que agrupa todas as funções de validação
  function validate(params) {
    squareMeterValidation();
    doorWindowAreaValidation();
    doorHeighValidation();
    // setando array de retorno da função getErrorsText para o state errorsToPrint
    setErrorsToPrint((prevState) => {
      return getErrorsText();
    });
  }

  // useEffect responsável por alterar o objeto/state 'cans', que é utilizado para renderizar a quantidade de latas sugeridas para pintar a parede.
  // a verificação das regras de negócio é feitas através do objeto/state 'errorsToPrint'.
  // se o objeto/state 'errorsToPrint' estiver vazio, todas as regras foram atentidas e um valor é para o objeto/state 'cans'
  // em caso de interação com uma api, uma requisiação HTTP(CRUD) poderia ser feita dentro desse useEffect.
  //  Ex: Utilizar o axios.post() dentro do 'IF' para salvar o resultado em uma api.
  useEffect(() => {
    if (errorsToPrint.length === 0) {
      setCans(paintQtt());
      window.scrollTo({ bottom: 0 });
    } else {
      window.scrollTo({ top: 0 });
      setCans("");
    }
  }, [errorsToPrint]);

  return (
    <div className="App">
      <header className="App-header"></header>
      {/* short circuit validation */}
      {showAlert && <AlertComponent errors={errorsToPrint} />}
      <Container>
        <Wall wallNumber={1} state={{ walls, setWalls }} />
        <Wall wallNumber={2} state={{ walls, setWalls }} />
        <Wall wallNumber={3} state={{ walls, setWalls }} />
        <Wall wallNumber={4} state={{ walls, setWalls }} />
      </Container>

      <Box sx={{ width: "100%" }}>
        <Stack
          direction={{ xs: "column", sm: "row" }}
          divider={<Divider orientation="vertical" flexItem />}
          spacing={2}
          justifyContent="center"
        >
          {cans}
        </Stack>
      </Box>
      <Box sx={{ width: "100%", display: "flex", justifyContent: "center" }}>
        <Button
          variant="contained"
          sx={{ width: { xs: "100%", lg: "50%" }, margin: 2 }}
          onClick={() => {
            validate();
            setShowAlert(true);
            // paintQtt();
          }}
        >
          Calcular
        </Button>
      </Box>
    </div>
  );
}

export default App;
