import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import InputValue from "./InputValue";

const Wall = (props) => {
  const { wallNumber, state } = props;

  // css criado como objetos para evitar poluição do código html
  const boxStyle = {
    display: "flex",
    justifyContent: "space-around",
    flexDirection: { xs: "column", lg: "row" },
    margin: 2,
  };

  const inputObjStyle = { m: 2 };

  //   Handlechange generico
  function handleChange(event) {
    state.setWalls({
      ...state.walls, //clone do state
      [`wall${wallNumber}`]: {
        //acesso dinamico de cada propriedade do state
        ...state.walls[`wall${wallNumber}`], // clone e escrita do valor de cada input do componente InputValue
        [event.target.name]: event.target.value,
      },
    });
  }

  return (
    <Box component="form" sx={boxStyle}>
      <Typography>Parede {wallNumber}</Typography>

      <InputValue
        variant="standard"
        inputType="number"
        label="Altura"
        name="height"
        sx={inputObjStyle}
        onChange={handleChange}
        value={state.walls[`wall${wallNumber}`].height}
      />
      <InputValue
        variant="standard"
        inputType="number"
        label="Largura"
        name="width"
        sx={inputObjStyle}
        onChange={handleChange}
        value={state.walls[`wall${wallNumber}`].width}
      />
      <InputValue
        variant="standard"
        inputType="number"
        label="Janela(s)"
        name="window"
        sx={inputObjStyle}
        onChange={handleChange}
        value={state.walls[`wall${wallNumber}`].window}
      />
      <InputValue
        variant="standard"
        inputType="number"
        label="Porta(s)"
        name="door"
        sx={inputObjStyle}
        onChange={handleChange}
        value={state.walls[`wall${wallNumber}`].door}
      />
    </Box>
  );
};

export default Wall;
