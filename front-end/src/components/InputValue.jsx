import { FormControl, TextField } from "@mui/material";
import React from "react";

// componente criado para evitar repetição do codigo <FormControl><TextField></TextField></FormControl>
// para cada Input do formulário
const InputValue = (props) => {
  const { variant, inputType, label, name, sx, onChange, value } = props;
  return (
    <FormControl variant={variant} sx={sx}>
      <TextField
        type={inputType}
        label={label}
        name={name}
        inputProps={{ min: 0 }}
        onChange={onChange}
        value={value}
      />
    </FormControl>
  );
};

export default InputValue;
