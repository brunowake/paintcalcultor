import { Alert, Typography } from "@mui/material";
import React from "react";

const AlertComponent = (props) => {
  const { errors } = props;

  return errors.length > 0 ? (
    <Alert severity="error">
      {errors.map((error, index) => (
        <Typography key={index}>{error}</Typography>
      ))}
    </Alert>
  ) : (
    <Alert severity="success">Calculo concluido com sucesso</Alert>
  );
};

export default AlertComponent;
