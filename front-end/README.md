O aplicativo está disponível online, o deploy foi feito na plataforma netlify e pode ser acessado através do link: https://paintcalculator.netlify.app/

Caso deseje executar o aplicativo localmente, o usuário deverá:

1- fazer um clone do repositório - usar o comando 'git clone https://gitlab.com/brunowake/paintcalcultor.git'

2- entrar na pasta 'front-end'

3- executar os comandos 'npm install' e 'npm start', respectivamente

Aplicativo construído em React Utilizando Material UI. Aplicativo Responsivo.
